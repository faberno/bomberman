import pickle
import matplotlib.pyplot as plt
rewards = []
import numpy as np

with open("progress.pt", "rb") as file:
    progress = pickle.load(file)

penalties = progress[0]
steps = progress[1]
score = progress[2]
avg_pen = progress[3]

x = np.arange(len(penalties))
plt.plot(x, penalties, label="penalty")
plt.plot(x, steps, label="Steps")
plt.legend()
plt.show()

plt.plot(x, score, label="Score")
plt.legend()
plt.show()

x = np.arange(len(avg_pen))
plt.plot(x, steps, label="Steps")
plt.plot(x, avg_pen, label="Avg Pen")
plt.legend()
plt.show()
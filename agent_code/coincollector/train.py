import pickle
from collections import namedtuple, deque
from typing import List
import numpy as np
import events as e
from .callbacks import look_for_targets, ACTIONS, state_to_features
from .parameters import TRANSITION_HISTORY_SIZE, LEARNING_RATE, DISCOUNT, game_rewards, SAVE_EVERY

# This is only an example!
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

# Events
COIN_LESSDISTANCE_EVENT = 'COIN_LESSDISTANCE'
COIN_SAMEDISTANCE_EVENT = 'COIN_SAMEDISTANCE'
COIN_MOREDISTANCE_EVENT = 'COIN_MOREDISTANCE'


def setup_training(self):
    """
    Initialise self for training purpose.

    This is called after `setup` in callbacks.py.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    """
    # Example: Setup an array that will note transition tuples
    # (s, a, r, s')
    self.all_rewards = []
    self.round_reward = 0
    self.highest_reward = -10000000
    self.transitions = deque(maxlen=TRANSITION_HISTORY_SIZE)



def eval_move(self, oldpos, newpos):
    target = self.target_coin
    diff = (newpos[0]- oldpos[0]+1, newpos[1]- oldpos[1]+1)
    if diff == (1,1):
        return COIN_SAMEDISTANCE_EVENT
    elif diff == target:
        return COIN_LESSDISTANCE_EVENT
    else:
        return COIN_MOREDISTANCE_EVENT


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    """
    Called once per step to allow intermediate rewards based on game events.

    When this method is called, self.events will contain a list of all game
    events relevant to your agent that occurred during the previous step. Consult
    settings.py to see what events are tracked. You can hand out rewards to your
    agent based on these events and your knowledge of the (new) game state.

    This is *one* of the places where you could update your agent.

    :param self: This object is passed to all callbacks and you can set arbitrary values.
    :param old_game_state: The state that was passed to the last call of `act`.
    :param self_action: The action that you took.
    :param new_game_state: The state the agent is in now.
    :param events: The events that occurred when going from  `old_game_state` to `new_game_state`
    """
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    if old_game_state is not None:
        old_features = self.current_features

        #see if agent moved in direction of coin
        if self.target_coin != None:
            events.append(eval_move(self, old_game_state['self'][3], new_game_state['self'][3]))

        if e.COIN_COLLECTED in events:
            #only delete target coin if this specific coin was collected
            if self.target_coin not in new_game_state['coins']:
                self.target_coin = None

        new_features = state_to_features(self, new_game_state)
        self.current_features = new_features

        reward = reward_from_events(self, events)
        transition = Transition(old_features, self_action, new_features, reward)

        if e.COIN_COLLECTED in events:
            self.q_table[old_features][ACTIONS.index(self_action)] = game_rewards[e.COIN_COLLECTED]
        else:
            max_future_q = np.max(self.q_table[new_features])
            current_q = self.q_table[old_features][ACTIONS.index(self_action)]

            new_q = current_q + LEARNING_RATE * (reward + DISCOUNT * max_future_q - current_q)
            self.q_table[old_features][ACTIONS.index(self_action)] = new_q
        self.round_reward += reward

        self.transitions.append(transition)


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    """
    Called at the end of each game or when the agent died to hand out final rewards.

    This is similar to reward_update. self.events will contain all events that
    occurred during your agent's final step.

    This is *one* of the places where you could update your agent.
    This is also a good place to store an agent that you updated.

    :param self: The same object that is passed to all of your callbacks.
    """
    self.transitions.append(
        Transition(state_to_features(self, last_game_state), last_action, None, reward_from_events(self, events)))
    # update the random probability
    self.random_prob = self.random_prob * self.random_decay
    round = last_game_state['round']
    self.all_rewards.append(self.round_reward)
    self.current_features = None
    self.round_reward = 0
    # Store the models

    if round == self.n_episodes or last_game_state['round'] % SAVE_EVERY == 0:
        with open("all-rewards.pt", "wb") as file:
            pickle.dump(self.all_rewards, file)
        with open(f"models/my-saved-models-{last_game_state['round']}.pt", "wb") as file:
            pickle.dump(self.q_table, file)


def reward_from_events(self, events: List[str]) -> int:
    """
    *This is not a required function, but an idea to structure your code.*

    Here you can modify the rewards your agent get so as to en/discourage
    certain behavior.
    """
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]
    return reward_sum
